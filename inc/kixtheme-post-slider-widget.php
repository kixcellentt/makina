<?php

/**
 * KixTheme Post Slider Widget
 * @package KixTheme Post Slider Widget
 * @author Frederick S. Crisostomo
 */
class KixTheme_Post_Slider_Widget extends WP_Widget {
    function __construct() {
        parent::__construct(
            'kixtheme_post_slider_widget',
            __('KixTheme Post Slider Widget','kixtheme'),
            array('description'=> __('Display posts as slider. The posts will be based on selected category','kixtheme'),)
        );
    }
    function update($new_instance,$old_instance){
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['columns'] = strip_tags($new_instance['columns']);
        $instance['category'] = strip_tags($new_instance['category']);
        return $instance;
    }
    function form($instance){
    if( $instance) {
        $title = esc_attr($instance['title']);
        $columns = esc_attr($instance['columns']);
        $category = esc_attr($instance['category']);
    } else {
        $title = '';
        $columns = '';
        $category = '';
    }
    ?>
    <p>
        <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'kixtheme_post_slider_widget'); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('columns'); ?>"><?php _e('Number of Columns:', 'kixtheme_post_slider_widget'); ?></label>
        <select id="<?php echo $this->get_field_id('columns'); ?>"  name="<?php echo $this->get_field_name('columns'); ?>">
                <?php for($x=1;$x<=6;$x++): ?>
                <option <?php echo $x == $columns ? 'selected="selected"' : '';?> value="<?php echo $x;?>"><?php echo $x; ?></option>
                <?php endfor;?>
        </select>
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Category:', 'kixtheme_post_slider_widget'); ?></label>
        <?php $categories =  get_categories();?>
        <select id="<?php echo $this->get_field_id('category'); ?>"  name="<?php echo $this->get_field_name('category'); ?>">
                <?php foreach($categories as $cat): ?>
                <option <?php echo $cat->name == $category ? 'selected="selected"' : '';?> value="<?php echo $cat->name;?>"><?php echo $cat->name; ?></option>
                <?php endforeach;?>
        </select>
    </p>
    <?php
    }

    /**
    * Front-end display of widget.
    *
    * @see WP_Widget::widget()
    *
    * @param array $args     Widget arguments.
    * @param array $instance Saved values from database.
    */
    public function widget( $args, $instance ) {
           echo $args['before_widget'];
           if ( ! empty( $instance['title'] ) ) {
                   echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
           }
           $this->getPostSlider($instance['columns'], $instance['category']);
           echo $args['after_widget'];
    }

    function getPostSlider($columns,$category) { //html
            global $post;
            $get_category = get_term_by('name', $category, 'category'); // get category data
            $slug = $get_category->slug; // get the category slug;
            query_posts(array('category_name' => $slug));
            add_image_size('image-slider-thumb', 710,450, true );
            if(have_posts() > 0) : ?>
            <div class="row post-slider margin-bottom-40">
                <div class="col-md-12">
                    <div class="owl-carousel owl-carousel2 kixtheme-post-slider-carousel" data-column="<?php echo $columns;?>">
                            <?php while(have_posts()): the_post(); ?>
                        <div class="post-slider-item">
                            <div <?php post_class("blog-item");?> id="post-<?php the_ID();?>">
                                <div class="blog-item-img">
                                    <div class="thumbnail">
                                        <?php the_post_thumbnail('image-slider-thumb');?>
                                    </div>
                                </div>
                                <h4><a href="<?php the_permalink();?>" rel="bookmark" title="Link to <?php the_title_attribute();?>"><?php the_title();?></a></h4>
                                <?php the_content();?>

                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div>       
                </div>
            </div>
            <?php	wp_reset_postdata();
            else :
                    echo '<p style="padding:25px;">No listing found</p>';
            endif;
    }
    
    
}
add_action( 'widgets_init', function(){ register_widget( 'KixTheme_Post_Slider_Widget' ); });

