<?php
/**
 * Makina Theme Options by KixTheme
 * 
 * @link https://codex.wordpress.org/Creating_Options_Pages
 *
 * @package Makina
 * @author Frederick S. Crisostomo
 */

/**
 * Add theme options menu under Theme > Appearance
 */

add_action( 'admin_menu', 'kixtheme_theme_page_add' );

function kixtheme_theme_page_add(){
    add_submenu_page( 'themes.php', 'Theme Options', 'Theme Options', 7, 'theme-options', 'kixtheme_theme_options_init' );
}

function kixtheme_enqueue_scripts(){
    wp_enqueue_media();
    wp_enqueue_script( 'media-uploader-script', get_template_directory_uri().'/assets/global/js/kixtheme-media-uploader.js' );
}

add_action( 'admin_enqueue_scripts', 'kixtheme_enqueue_scripts' );

function kixtheme_theme_options_init() {
    ?>
    
    <!-- Create a header in the default WordPress 'wrap' container -->
    <div class="wrap">
     
        <div id="icon-themes" class="icon32"></div>
        <h2>Theme Options</h2>
        <?php settings_errors(); ?>
         
        <?php
            if( isset( $_GET['tab'] ) ) {
                $active_tab = $_GET['tab'];
            } else {
                $active_tab = 'general';
            }
            // end if
        ?>
         
        <h2 class="nav-tab-wrapper">
            <a href="?page=theme-options&tab=general" class="nav-tab <?php echo 'general' == $active_tab  ? 'nav-tab-active' : '' ; ?>">General</a>
            <a href="?page=theme-options&tab=image-slider" class="nav-tab <?php echo 'image-slider' == $active_tab ? 'nav-tab-active' : '' ; ?>">Image Slider</a>
            <a href="?page=theme-options&tab=social-media" class="nav-tab <?php echo 'social-media' == $active_tab ? 'nav-tab-active' : '' ; ?>">Social Media</a>
        </h2>

        
        <form method = "post" action = "options.php" >
            <?php 
                if( 'general' == $active_tab ) {
                    settings_fields( 'general_options_group' );
                    do_settings_sections( 'general_options_group' );
                } elseif( 'image-slider' == $active_tab ) {
                    settings_fields( 'image_slider_options_group' ); 
                    do_settings_sections( 'image_slider_options_group' );
                } else {
                    settings_fields( 'social_media_options_group' ); 
                    do_settings_sections( 'social_media_options_group' );
                }
            ?> 
         
            <?php submit_button(); ?>
             
        </form>
         
    </div><!-- /.wrap -->
<?php
} /*end function kixtheme_theme_options_init() */

/**
 * BEGIN GENERAL OPTIONS
 */
/**
 * Create form fields for General Options
 */
function form_general_options( array $args ){
   
    /*Get the theme colors from JSON file */
    $json = file_get_contents( get_template_directory()."/assets/global/css/themes/theme-colors.json" );
    $array = json_decode( $json, true );
?>
    <!-- Form: Theme Color -->
    <?php if ( 'theme_color' == $args['form'] ) : ?>
        <select name="theme_color" id="theme_color" class="regular-text">
            <?php foreach ( $array['theme_colors'] as $theme_colors ) : ?>
                <option value="<?php echo $theme_colors['name']; ?>" <?php echo get_option( 'theme_color' ) == $theme_colors['name'] ? "Selected='selected'": null; ?> style="background : <?php echo $theme_colors['color']; ?>"><?php echo $theme_colors['label']; ?></option>
            <?php endforeach; //$array['theme_colors'] as $theme_colors ?>
        </select>
    
    <!-- Form: Contact Number -->
    <?php else :?>
        <input type="text" name="contact_number" id="contact_number" value="<?php echo get_option( 'contact_number' ); ?>" />
<?php endif; /* end if($args['form']== ???) */

} /*End of function form_general_options*/


/**
 * Add section and register the created form fields to the section
 */
function kixtheme_general_options() {
    add_settings_section(
        "general_section",      // id
        "General Options",      // title
        null,                   // callback
        "general_options_group" // page
    );
    
    add_settings_field(
        "theme_color",                      // id
        "Theme Color",                      // title
        "form_general_options",             // callback
        "general_options_group",            // page
        "general_section",                  // section
        array( 'form' => 'theme_color' )    // args
    );
    add_settings_field(
        "contact_number",                   // id
        "Contact Number",                   // title
        "form_general_options",             // callback
        "general_options_group",            // page
        "general_section",                  // section
        array( 'form' => 'contact_number' ) // args
    );
    
    register_setting( "general_options_group", "theme_color" );
    register_setting( "general_options_group", "contact_number" );
}

/* Hook the function to an action */
add_action( "admin_init", "kixtheme_general_options" );

/**
 * END GENERAL OPTIONS
 */

/************************************************************************************************************************/

/**
 * BEGIN IMAGE SLIDER OPTION
 */

/**
 * Create form fields for Image Slider Options
 */

$max_image_slider = 5;

function form_image_slider( array $args ) {
    
    global $max_image_slider;
?>
    <!-- Form: Image Slider On -->
    <?php if( 'image_slider_on' == $args['form'] ) : ?>
    
        <!-- Off -->
        <input type="radio" name="image_slider_on" id="image_slider_on" value="off" 
            <?php echo 'off' == get_option( 'image_slider_on' ) ? "Checked='checked'" : null; ?>/> 
            Off 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
        <!-- Frontpage only -->
        <input type="radio" name="image_slider_on" id="image_slider_on" value="frontpage" 
            <?php echo 'frontpage' == get_option( 'image_slider_on' ) ? "Checked='checked'" : null; ?> />
            Front page Only 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
        <!-- All pages -->
        <input type="radio" name="image_slider_on" id="image_slider_on" value="all" 
            <?php echo 'all' == get_option( 'image_slider_on' ) ? "Checked='checked'" : null;?> />
            All pages
        
            
    <!-- Form: Image Slider On -->
    <?php elseif ( 'image_slider_settings' == $args['form'] ) : ?>
    
        <!-- Image Only --> 
        <input type="radio" name="image_slider_settings" id="image_slider_settings" value="1" 
            <?php echo 1 == get_option( 'image_slider_settings' ) ? "Checked='checked'" : null; ?> /> 
            Image Only &nbsp;&nbsp;
            <i>(Images will be set as background)</i>
            <br/>
            
        <!-- Image with caption -->
        <input type="radio" name="image_slider_settings" id="image_slider_settings" value="2" 
            <?php echo 2 == get_option( 'image_slider_settings' ) ? "Checked='checked'" : null; ?> />
            Image with caption &nbsp;&nbsp;
            <i>(Images will transitioned together with captions)</i>
            <br/>
            
        <!-- Image as background with caption -->
        <input type="radio" name="image_slider_settings" id="image_slider_settings" value="3" 
            <?php echo 3 == get_option( 'image_slider_settings' ) ? "Checked='checked'" : null; ?> />
            Image as background with caption &nbsp;&nbsp;
            <i>(Images will set as background, only the captions will be transitioned)</i>
            
    <?php endif; /* End if ( ??? == $args['form'] ) */ ?>
    
    
    <?php for( $i = 1; $i <= $max_image_slider; $i++ ) : ?>
        
        <!-- Form: Image URL -->
        <?php if( 'image_slider_url'.$i == $args['form'] ) : ?>
            <input type="text" name="image_slider_url<?php echo $i; ?>" id="image_slider_url<?php echo $i;?>" class="regular-text" value="<?php echo get_option( 'image_slider_url'.$i ); ?>">
            <input type="button" name="upload-btn<?php echo $i;?>" id="upload-btn<?php echo $i;?>" value="Upload Image" onclick="kixtheme_upload_media( '#image_slider_url<?php echo $i; ?>' );" />
            
        <!-- Form: Image Size -->
        <?php elseif( 'image_slider_img_size'.$i == $args['form'] ) :?>
            <input type="text" name="image_slider_img_size<?php echo $i; ?>" id="image_slider_img_size<?php echo $i; ?>" class="medium-text" value="<?php echo get_option( 'image_slider_img_size'.$i ); ?>" />
            &nbsp;&nbsp;
            <i>(height, width)</i>
            
        <!-- Form: Image Position -->
        <?php elseif( 'image_slider_img_pos'.$i == $args['form'] ) :?>
            <input type="text" name="image_slider_img_pos<?php echo $i; ?>" id="image_slider_img_pos<?php echo $i;?>" class="medium-text" value="<?php echo get_option( 'image_slider_img_pos'.$i ); ?>" />
            &nbsp;&nbsp;
            <i>(x-pos, y-pos)</i>
            
        <!-- Form: Caption Title -->
        <?php elseif($args['form']=='image_slider_title'.$i): ?>
            <input type="text" name="image_slider_title<?php echo $i;?>" id="image_slider_title<?php echo $i;?>" class="regular-text" value="<?php echo get_option('image_slider_title'.$i);?>" />
            
        <!-- Form: Caption Title Position -->
        <?php elseif( 'image_slider_title_pos'.$i == $args['form'] ) : ?>
            <input type="text" name="image_slider_title_pos<?php echo $i; ?>" id="image_slider_title_pos<?php echo $i; ?>" class="medium-text" value="<?php echo get_option( 'image_slider_title_pos'.$i ); ?>" />
            &nbsp;&nbsp;
            <i>(x-pos, y-pos)</i>
            
        <!-- Form: Caption Subtitle -->
        <?php elseif ( 'image_slider_subtitle'.$i == $args['form'] ) : ?>
            <input type="text" name="image_slider_subtitle<?php echo $i;?>" id="image_slider_subtitle<?php echo $i;?>" class="regular-text" value="<?php echo get_option('image_slider_subtitle'.$i);?>" />
            
        <!-- Form: Caption Subtitle Position -->
        <?php elseif ( 'image_slider_subtitle_pos'.$i == $args['form'] ) : ?>
            <input type="text" name="image_slider_subtitle_pos<?php echo $i; ?>" id="image_slider_subtitle_pos<?php echo $i; ?>" class="medium-text" value="<?php echo get_option( 'image_slider_subtitle_pos'.$i ); ?>" />
            &nbsp;&nbsp;
            <i>(x-pos, y-pos)</i>
            
        <!-- Form: Caption Button -->
        <?php elseif ( 'image_slider_button'.$i == $args['form'] ) : ?>
            <input type="text" name="image_slider_button<?php echo $i; ?>" id="image_slider_button<?php echo $i; ?>" value="<?php echo get_option( 'image_slider_button'.$i ); ?>" />
            
        <!-- Form: Caption Button Position -->
        <?php elseif ( 'image_slider_button_pos'.$i == $args['form'] ) : ?>
            <input type="text" name="image_slider_button_pos<?php echo $i; ?>" id="image_slider_button_pos<?php echo $i; ?>" value="<?php echo get_option( 'image_slider_button_pos'.$i ); ?>" />
            &nbsp;&nbsp;
            <i>(x-pos, y-pos)</i>
            
        <!-- Form: Transition Effect -->
        <?php elseif ( 'image_slider_transition'.$i == $args['form'] ) : ?>
            <input type="radio" name="image_slider_transition<?php echo $i;?>" id="image_slider_transition<?php echo $i; ?>" value="lfl" 
                <?php echo ( "lfl" == get_option( 'image_slider_transition'.$i ) ) ? "Checked='checked'" : null; ?> />
            From Left 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <input type="radio" name="image_slider_transition<?php echo $i;?>" id="image_slider_transition<?php echo $i; ?>" value="lfr" 
                <?php echo ( "lfr" == get_option( 'image_slider_transition'.$i ) ) ? "Checked='checked'" : null; ?> />
            From Right
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <input type="radio" name="image_slider_transition<?php echo $i; ?>" id="image_slider_transition<?php echo $i; ?>" value="lft" 
                <?php echo ( "lft" == get_option( 'image_slider_transition'.$i ) ) ? "Checked='checked'" : null; ?> /> 
            From Top 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <input type="radio" name="image_slider_transition<?php echo $i; ?>" id="image_slider_transition<?php echo $i; ?>" value="lfb" 
                <?php echo ( "lfb" == get_option( 'image_slider_transition'.$i ) ) ? "Checked='checked'" : null; ?> /> 
            From Bottom
            
    <?php endif; /*end if( ??? == $args['form'] ) */ ?>
            
    <?php endfor; /* end for( $i = 1; $i <= $max_image_slider; $i++ ) */ ?>
            
<?php
} /* End of creating form fields for Image Slider Option */

/**
 * Add Sections and register the created form fields to each section
 */
function kixtheme_image_slider_options(){
    
    global $max_image_slider;
    /**
     * Add Section Image Slider Options and register fields to this section
     */
    add_settings_section(
        "image_slider_on_section",      // id
        "Image Slider Options",         // title
        null,                           // callback
        "image_slider_options_group"    // page
        );
    
    add_settings_field(
        "image_slider_on",                          // id
        "Image Slider On",                          // title
        "form_image_slider",                        // callback
        "image_slider_options_group",               // page
        "image_slider_on_section",                  // section
        array( 'form' => 'image_slider_on' )        // args
        ) ;
    add_settings_field(
        "image_slider_settings",                    // id
        "Image Slider Settings",                    // title
        "form_image_slider",                        // callback
        "image_slider_options_group",               // page
        "image_slider_on_section",                  // section
        array( 'form' => 'image_slider_settings' )  // args
        );
    
    register_setting( "image_slider_options_group", "image_slider_on" );
    register_setting( "image_slider_options_group", "image_slider_settings" );
    
    /* Loop through the images */
    for( $i = 1; $i <= $max_image_slider; $i++ ) {
    /**
     * Add Section per image and register fields to each section
     */    
    add_settings_section(
        "image".$i."_section",          // id
        "Image #".$i,                   // title
        null,                           // callback
        "image_slider_options_group"    // page
        );
    
    add_settings_field(
        "image_slider_url".$i,                          // id
        "Image",                                        // title
        "form_image_slider",                            // callback
        "image_slider_options_group",                   // page
        "image".$i."_section",                          // section
        array( 'form' => 'image_slider_url'.$i )        // args
        );
    add_settings_field(
        "image_slider_img_size".$i,                     // id
        "Image Size",                                   // title
        "form_image_slider",                            // callback
        "image_slider_options_group",                   // page
        "image".$i."_section",                          // section
        array( 'form' => 'image_slider_img_size'.$i )   // args
        );
    add_settings_field(
        "image_slider_img_pos".$i,                      // id
        "Image Position",                               // title
        "form_image_slider",                            // callback
        "image_slider_options_group",                   // page
        "image".$i."_section",                          // section
        array( 'form' => 'image_slider_img_pos'.$i )    // args
        );
    add_settings_field(
        "image_slider_title".$i,                        // id
        "Title",                                        // title
        "form_image_slider",                            // callback
        "image_slider_options_group",                   // page
        "image".$i."_section",                          // section
        array( 'form' => 'image_slider_title'.$i )      // args
        );
    add_settings_field(
        "image_slider_title_pos".$i,                    // id
        "Title Position",                               // title
        "form_image_slider",                            // callback
        "image_slider_options_group",                   // page
        "image".$i."_section",                          // section
        array( 'form' => 'image_slider_title_pos'.$i )  // args
        );
    add_settings_field(
        "image_slider_subtitle".$i,                     // id
        "Sub-title",                                    // title
        "form_image_slider",                            // callback
        "image_slider_options_group",                   // page
        "image".$i."_section",                          // section
        array( 'form' => 'image_slider_subtitle'.$i )   // args
        );
    add_settings_field(
        "image_slider_subtitle_pos".$i,                     // id
        "Sub-title Position",                               // title
        "form_image_slider",                                // callback
        "image_slider_options_group",                       // page
        "image".$i."_section",                              // section
        array( 'form' => 'image_slider_subtitle_pos'.$i )   // args
        );
    add_settings_field(
        "image_slider_button".$i,                       // id
        "Button",                                       // title
        "form_image_slider",                            // callback
        "image_slider_options_group",                   // page
        "image".$i."_section",                          // section
        array('form'=>'image_slider_button'.$i));       // args
    add_settings_field(
        "image_slider_button_pos".$i,                   // id
        "Button Position",                              // title
        "form_image_slider",                            // callback
        "image_slider_options_group",                   // page
        "image".$i."_section",                          // section
        array('form'=>'image_slider_button_pos'.$i)     // args
        );
    add_settings_field(
        "image_slider_transition".$i,                   // id
        "Transition",                                   // title
        "form_image_slider",                            // callback
        "image_slider_options_group",                   // page
        "image".$i."_section",                          // section
        array('form'=>'image_slider_transition'.$i)     // args
        );
    
    register_setting( "image_slider_options_group", "image_slider_url".$i );
    register_setting( "image_slider_options_group", "image_slider_img_size".$i );
    register_setting( "image_slider_options_group", "image_slider_img_pos".$i );
    register_setting( "image_slider_options_group", "image_slider_title".$i );
    register_setting( "image_slider_options_group", "image_slider_title_pos".$i );
    register_setting( "image_slider_options_group", "image_slider_subtitle".$i );
    register_setting( "image_slider_options_group", "image_slider_subtitle_pos".$i );
    register_setting( "image_slider_options_group", "image_slider_button".$i );
    register_setting( "image_slider_options_group", "image_slider_button_pos".$i );
    register_setting( "image_slider_options_group", "image_slider_transition".$i );
    
    } /* End of image loop*/
    
} /* End of function kixtheme_image_slider_options */

/* Hook the function to an action */
add_action("admin_init", "kixtheme_image_slider_options");

/**
 * END IMAGE SLIDER OPTION
 */
/************************************************************************************************************************/

/**
 * BEGIN SOCIAL MEDIA OPTIONS
 */
/**
 * Create form fields for Social Media Options
 */
/*Get the theme colors from JSON file */
    $json = file_get_contents( get_template_directory()."/social-media-accounts.json" );
    $social_media = json_decode( $json, true );
function form_social_media_options( array $args ){
   global $social_media;
?>
    <!-- Form: Social Media  -->
    <?php foreach ( $social_media['accounts'] as $account ) : ?>
        <?php if ( $account['id'] == $args['form'] ) : ?>
    <input type="url" name="<?php echo $account['id']; ?>" id="<?php echo $account['id']; ?>" value="<?php echo get_option( $account['id'] ); ?>" class="regular-text"/>
        <?php endif; /* end if( ??? == $args['form'] ) */ ?>
    <?php endforeach; //$social_media['account'] as $account 
} /*End of function form_social_media_options*/


/**
 * Add section and register the created form fields to the section
 */
function kixtheme_social_media_options() {
    global $social_media;
    
    add_settings_section(
        "social_media_section",         // id
        "Social Media Accounts",        // title
        null,                           // callback
        "social_media_options_group"    // page
    );
    
    foreach( $social_media['accounts'] as $account ) {
        add_settings_field(
            $account['id'],                         // id
            $account['title'],                      // title
            "form_social_media_options",            // callback
            "social_media_options_group",           // page
            "social_media_section",                 // section
            array( 'form' => $account['id'] )       // args
        );
        register_setting( "social_media_options_group", $account['id'] );
    }
}

/* Hook the function to an action */
add_action( "admin_init", "kixtheme_social_media_options" );

/**
 * END SOCIAL MEDIA OPTIONS
 */

/************************************************************************************************************************/