<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Makina
 */

if ( ! function_exists( 'kixtheme_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function kixtheme_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		null,
		null
	);
//	$time_string = sprintf( $time_string,
//		esc_attr( get_the_date( 'c' ) ),
//		esc_html( get_the_date() ),
//		esc_attr( get_the_modified_date( 'c' ) ),
//		esc_html( get_the_modified_date() )
//	);

	$posted_on = sprintf(
		'<i class="fa fa-calendar"></i> '.esc_html_x( ' %s', 'post date', 'kixtheme' ),
		'<a href="' . esc_url( get_permalink() ) . '" class="w3-hover-text-theme-d5" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		'<i class="fa fa-user"></i> '.esc_html_x( ' %s', 'post author', 'kixtheme' ),
		'<span class="author vcard w3-hover-text-theme-d5"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . ' ">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on w3-text-theme">' . $posted_on . '</span>&nbsp;&nbsp;<span class="byline w3-text-theme"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'kixtheme_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function kixtheme_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'kixtheme' ) );
		if ( $categories_list && kixtheme_categorized_blog() ) {
			printf( '<span class="cat-links w3-text-theme">' . esc_html__( 'Posted in %1$s', 'kixtheme' ) . '</span>', '<span class="w3-text-theme-d3 w3-hover-text-theme-d5">'.$categories_list.'</span>' ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'kixtheme' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links w3-text-theme">' . esc_html__( 'Tagged %1$s', 'kixtheme' ) . '</span>', '<span class="w3-text-theme-d3 w3-hover-text-theme-d5">'.$tags_list.'</span>' ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		/* translators: %s: post title */
		comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'kixtheme' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
                    '<i class="fa fa-pencil"></i> '.esc_html__( 'Edit %s', 'kixtheme' ),
                    null
		),
		'<span class="edit-link w3-text-theme-d1 w3-hover-text-theme-d5 w3-container">',
		'</span>'
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function kixtheme_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'kixtheme_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'kixtheme_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so kixtheme_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so kixtheme_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in kixtheme_categorized_blog.
 */
function kixtheme_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'kixtheme_categories' );
}
add_action( 'edit_category', 'kixtheme_category_transient_flusher' );
add_action( 'save_post',     'kixtheme_category_transient_flusher' );

/**
 * Customize the comment list
 */
function kixtheme_comment_list($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>
    <li <?php comment_class("w3-panel w3-leftbar w3-theme-l5 padding-top-10"); ?> id="li-comment-<?php comment_ID(); ?>">
        <div id="comment-<?php comment_ID(); ?>">
            <div class="comment-author vcard">
                <?php echo get_avatar( $comment, 32 ); ?>
                <?php comment_author_link(); ?>
            </div>
            
            <?php if( 0 == $comment->comment_approved ) : ?>
            <em> Your comment is awaiting approval. </em> <br/>
            <?php endif; ?>
            
            <div class="comment-time margin-top-10 w3-text-theme">
                <a href="<?php echo htmlspecialchars(get_comment_link( $comment->comment_ID ) ); ?>" class="w3-hover-text-theme-d5">
                    <?php comment_date(); ?> at <?php comment_time(); ?>
                </a>
                <?php edit_comment_link( 'Edit', ' | ', '' ); ?>
                | <?php comment_reply_link(array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'], 'login_text' => 'Login to Reply' ) ) ) ?>
            </div>
            <div class="">
                <?php comment_text(); ?>
            </div>
        </div>
    </li>
<?php  
}

/**
 * Add class to edit comment link
 * @param type $output
 */
function kixtheme_edit_comment_link( $output ) {
  return preg_replace( '/comment-edit-link/', 'comment-edit-link w3-hover-text-theme-d5', $output, 1 );
}
add_filter( 'edit_comment_link', 'kixtheme_edit_comment_link' );

/**
 * Add class to reply comment link
 * @param type $output
 */
function kixtheme_comment_reply_link( $output ) {
    return preg_replace( '/comment-reply-link/', 'comment-reply-link w3-hover-text-theme-d5', $output, 1 );
}
add_filter( 'comment_reply_link', 'kixtheme_comment_reply_link' );
