<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Humane
 */
get_header();
?>
    <main class="main">
        <div class="row w3-theme-l5">
<!--            <div class="w3-card-2">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/global/img/image_slider/banner-01.jpg" width="100%"/>
            </div>-->
            <?php 
                get_template_part( 'template-parts/class-image-slider' ); 
                Image_Slider::init();
            ?>
        </div>
        <?php
            get_template_part( 'template-parts/class-main-widget' );
            Main_Widget::init();
        ?>
    </main>
<?php
get_footer();