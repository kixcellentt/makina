jQuery(document).ready(function(){
    
    /**
     * Post Slider Carousel
     * This is the carousel for post slider widget
     */
    $('.kixtheme-post-slider-carousel').each(function(){
        $(this).owlCarousel({
        items: $(this).attr("data-column") ? $(this).attr("data-column") : 3,
        //Basic Speeds
        slideSpeed : 200,
        paginationSpeed : 800,
        rewindSpeed : 1000,

        //Autoplay
        autoPlay : true,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: false,

        // Navigation
        navigation : true,
        navigationText : ["prev","next"],
        rewindNav : true,
        scrollPerPage : false

        //Pagination
//        pagination : true,
//        paginationNumbers: true,
        
        });
    })
    
});