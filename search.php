<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Makina
 */

get_header(); ?>

	<main class="main w3-white">
            <div class="container">
                <div class="w3-row-padding  margin-top-15 margin-bottom-15">
                    <div class="w3-col <?php echo ( ! is_active_sidebar( 'sidebar-1' ) && ! is_active_sidebar( 'sidebar-2' ) ) ? 'm12 s12' : 'm9 s12' ?>">
                        <div class="w3-card-4 w3-container">
		<?php
		if ( have_posts() ) : ?>

			<header class="page-header w3-text-theme-d5">
				<h3 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'kixtheme' ), '<span>' . get_search_query() . '</span>' ); ?></h4>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );
                                echo "<hr/>";
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</div>
                    </div>
                    <div class="w3-col m3 s12">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
	</main>

<?php
get_sidebar();
get_footer();
