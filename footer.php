<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makina
 */
?>
    <footer>
        <!-- BEGIN PRE-FOOTER -->
        <?php 
            get_template_part( 'template-parts/class-footer-widget' );
            Footer_Widget:: init(); 
        ?>
        <!-- END PRE-FOOTER -->
        <div class="footer w3-theme-dark">
          <div class="container">
            <div class="row">
              <!-- BEGIN COPYRIGHT -->
              <div class="col-md-6 col-sm-6 padding-top-10">
                  2016 © MAKINA. All Rights Reserved.
                  <br/>
                  Makina Theme by <a href="http://kixcellent.com/kixtheme" class="w3-hover-text-theme-l1">KixTheme</a>
              </div>
              <!-- END COPYRIGHT -->
            </div>
          </div>
        </div>
    </footer>
    <!-- END FOOTER -->

    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>      
    <script src="<?php echo get_template_directory_uri(); ?>/js/back-to-top.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/assets/global/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/js/kixtheme-owl-carousel.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.plugins.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script> 
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script> 
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/js/revo-slider-init.js" type="text/javascript"></script>
    <!-- END RevolutionSlider -->

    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/js/layout.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/js/custom.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery( document ).ready( function() {
            Layout.init();
            RevosliderInit.initRevoSlider();
            Layout.initTwitter();
            Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
            Layout.initNavScrolling();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
<?php wp_footer(); ?>

</body>
</html>
