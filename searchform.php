<?php
/**
 * 
 */
?>
<form action="<?php echo home_url( '/' ); ?>" method="get" class="input-group" style="min-width:250px!important;">
    <label for="search"></label>
    <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" class="form-control"/>
    <span class="input-group-btn">
        <button type="submit" class="btn w3-theme-d5"><i class="fa fa-search"></i></button>
    </span>
</form>
