<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until main content
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makina
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
<link href="<?php echo get_template_directory_uri();?>/assets/global/css/themes/<?php echo get_option( 'theme_color' ) ? get_option( 'theme_color' ) : 'platinum'; ?>.css" rel="stylesheet">
<!--<link href="<?php echo get_template_directory_uri();?>assets/global/css/custom.css" rel="stylesheet">-->
<?php if ( is_singular() ) wp_enqueue_script( 'comment_reply' ); ?>
<?php wp_head(); ?>
</head>

<body>
    <header class="header w3-white">
        <!-- BEGIN TOP BAR -->
<!--        <div class="top-header">
            <div class="container">
                <div class="row">
                     BEGIN TOP BAR LEFT PART 
                    <div class="col-md-6 col-sm-6">
                        
                    </div>
                     END TOP BAR LEFT PART 
                     BEGIN TOP BAR MENU 
                    <div class="col-md-6 col-sm-6 additional-nav">
                        
                    </div>
                     END TOP BAR MENU 
                </div>
            </div>        
        </div>-->
        <!-- END TOP BAR -->
        <!-- BEGIN HEADER -->
        <div class="main-header w3-white">
          <div class="container">
                <div class="row">
                    <div class="col-md-2 padding-top-25">
                        <a class="site-logo w3-text-theme" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
                    </div>
                    <div class="col-md-4 pull-right padding-top-20 padding-bottom-5">
                        <div class="socicon">
                            <?php 
                                $json = file_get_contents( get_template_directory()."/social-media-accounts.json" );
                                $social_media = json_decode( $json, true );
                            ?>
                            <?php foreach( $social_media['accounts'] as $account ) : ?>
                                <?php if( get_option( $account['id'] ) ) : ?>
                                    <a class="socicon-btn socicon-<?php echo strtolower( $account['title'] ); ?> tooltips w3-text-theme w3-border-theme social-media-hover-text-<?php echo strtolower( $account['title'] );?> social-media-hover-border-<?php echo strtolower( $account['title'] );?> w3-hover-shadow" data-original-title="<?php echo strtolower( $account['title'] ); ?>" href="<?php echo get_option( $account['id'] ); ?>" target="_blank"> </a>
                                <?php endif; /*if( get_option( $account['id'] ) )*/?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-md-4 pull-right padding-top-25">
                        <ul class="list-unstyled list-inline">
                            <li><a href="tel:<?php echo str_replace( array( '-' , ' ', '(', ')' ), '', get_option( 'contact_number' ) );?>" class="w3-text-theme w3-hover-text-theme-d3"><i class="fa fa-phone"></i><span><?php echo get_option( 'contact_number' ); ?></span></a></li>
                            <li><a href="mailto:<?php bloginfo( 'admin_email' );?>" class="w3-text-theme w3-hover-text-theme-d3"><i class="fa fa-envelope-o"></i><span><?php bloginfo( 'admin_email' ); ?></span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- END NAVIGATION -->
                
                <!-- END NAVIGATION -->
            </div>
            <nav class="row navbar w3-theme-dark margin-bottom-0">
                <div class="container">
                    <div class="navbar-header">
                        <a class="navbar-toggle w3-text-white pull-right" data-target=".navbar-collapse" data-toggle="collapse">
                                <i class="fa fa-bars"></i>
                            </a>
                    </div>
                    <div class="collapse navbar-collapse">
<!--                         <ul class="w3-navbar w3-col m9">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Link 1</a></li>
                            <li class="w3-dropdown-hover">
                              <a href="#">Dropdown <i class="fa fa-caret-down"></i></a>
                              <ul class="w3-dropdown-content w3-white w3-card-4">
                                  <li>
                                      <a href="#">Link 1</a>
                                  </li>
                                  <li>
                                      <a href="#">Link 2</a>
                                  </li>
                                  <li class="w3-dropdown-submenu">
                                      <a href="#">Link 3</a>
                                      <ul class="w3-dropdown-content">
                                          <li><a href="#">Link 1</a></li>
                                          <li><a href="#">Link 2</a></li>
                                          <li class="w3-dropdown-submenu">
                                                <a href="#">Link 3</a>
                                                <ul class="w3-dropdown-content">
                                                    <li><a href="#">Link 1</a></li>
                                                    <li><a href="#">Link 2</a></li>
                                                    <li><a href="#">Link 3</a></li>
                                                </ul>
                                            </li>
                                      </ul>
                                  </li>
                              </ul>
                            </li>
                          </ul>-->
                        <?php
                            include( 'w3css-walker.php' );
                            wp_nav_menu(
                                array(
                                    'theme_location'    =>  'primary',
                                    'container_class'   =>  'w3-col m9 no-padding',
                                    'menu_class'        =>  'nav navbar-nav navbar-left',
                                    'depth'             =>  0,
                                    'walker'            =>  new W3CSS_Walker_Nav_Menu(),
        //                                    'fallback_cb'       => 'humane_wp_page_menu'
                                )
                            );
                        ?>
                        <div class="w3-col m3 no-padding">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" data-toggle="dropdown" class="w3-padding-24 w3-text-theme w3-hover-theme-d5"><i class="fa fa-search"></i></a>
                                    <ul class="dropdown-menu">
                                        <div class="w3-text-theme-d5"><?php get_search_form(); ?></div>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav">
                                <?php if( !is_user_logged_in() ) : ?> 
                                <li><a href="<?php echo site_url(); ?>/wp-login.php" class="w3-text-theme w3-hover-text-theme-d3 w3-padding-24">Log In</a></li>
                                <li><a href="<?php echo site_url(); ?>/wp-register.php" class="w3-text-theme w3-hover-text-theme-d3 w3-padding-24">Registration</a></li>
                                <?php endif; ?>
                            </ul>
                        
                        </div>
                    </div>
                
                </div>
            </nav>
        </div>
        <!-- Header END -->
    </header>