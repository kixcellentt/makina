<?php
/**
 * Description of Main_Widget_Functions
 *
 * @author red
 */
class Main_Widget {
    public function init()
    {
        $parts = 0;
        $widget_array = array(
            'main-top' => array('main-top-1','main-top-2','main-top-3','main-top-4'),
            'main-middle' => array('main-middle-1','main-middle-2','main-middle-3','main-middle-4'),
            'main-bottom' => array('main-bottom-1','main-bottom-2','main-bottom-3','main-bottom-4'),
        );
        foreach($widget_array as $widgets)
        {
            $widget_col_class = "col-md-12 col-sm-12";
            $widget_count = 0;
            foreach($widgets as $widget){
                if(is_active_sidebar($widget)) {$widget_count = $widget_count+1;}
            }
            switch($widget_count){
                case 4: $widget_col_class = "col-md-3 col-sm-3";
                    break;
                case 3: $widget_col_class = "col-md-4 col-sm-4";
                    break;
                case 2: $widget_col_class = "col-md-6 col-sm-6";
                    break;
                default: $widget_col_class = $widget_col_class;
                    break;
            }
            if($widget_count>0){
                $parts++;
                $row_class = ($parts % 2) ? "": "w3-theme-l5";
                echo "<div class='row $row_class padding-top-20 padding-bottom-20'>";
                echo "<div class='container'>";
                /*loop through widgets*/
                foreach($widgets as $widget){
                    if(is_active_sidebar($widget)){
                        echo "<div class='$widget_col_class widget-area' role='complementary'>";
                        dynamic_sidebar($widget);
                        echo "</div>";
                    }
                }
                echo "</div>";
                echo "</div>";
            }
        }
    }
}
