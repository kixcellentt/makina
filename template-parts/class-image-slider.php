<?php

/**
 * KixTheme Image Slider
 * @package KixTheme Image Slider
 * @author Frederick S. Crisostomo
 */
class Image_Slider {
    public function init() {
        if ( get_option( 'image_slider_on' ) ) : ?>
            <!-- BEGIN SLIDER -->
            <div class="page-slider margin-bottom-0">
                <div class="fullwidthbanner-container revolution-slider">
                    <div class="fullwidthabnner w3-theme-l3" data-start="600">
                        <ul id="revolutionul">
                            <!-- LOOP THROUGH IMAGES -->
                            <?php for ( $i = 1; $i <= 5; $i++ ) : ?>
                                
                                <?php
                                    /* If there's an image */
                                    if ( get_option( 'image_slider_url'.$i ) ) : 
                                ?> 
                            
                                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="<?php echo get_option( 'image_slider_url'.$i ); ?>" >
                                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                                        <?php
                                            /* If the selected option is 'Image Only' OR 'Image as background with caption' */
                                            if( 1 == get_option('image_slider_settings') OR 3 == get_option('image_slider_settings') ) : 
                                        ?>
                                            <!-- Set the image as background -->
                                            <img src="<?php echo get_option( 'image_slider_url'.$i ); ?>" alt="Image<?php echo $i; ?>" >
                                        <?php endif; ?>
                                        
                                        <?php
                                            /* If the selected option is 'Image with caption' OR 'Image as background with caption' */
                                            if ( 2 == get_option( 'image_slider_settings' ) OR 3 == get_option( 'image_slider_settings' ) ) : 
                                        ?>
                                            <?php
                                                /* If the selected option is 'Image with caption' */
                                                if( 2 == get_option( 'image_slider_settings' ) ) :
                                                    /* set default values of variables */
                                                    $height = 500;
                                                    $width = "auto";
                                                    $img_xpos = 400;
                                                    $img_ypos = 0;
                                                    /* If image size is set, change the values of size variables */
                                                    if ( get_option( 'image_slider_img_size'.$i ) ) {
                                                        $img_size = explode( ',', get_option( 'image_slider_img_size'.$i ) );
                                                        $height = ( $img_size[0] OR $img_size[0]>0 ) ? $img_size[0] : $height;
                                                        $width = ( $img_size[1] OR $img_size[1] > 0 ) ? $img_size[1] : $width;
                                                    }
                                                    /* If image position is set, change the values of position variables */
                                                    if ( get_option( 'image_slider_img_pos'.$i ) ) {
                                                        $img_pos = explode( ',', get_option( 'image_slider_img_pos'.$i ) );
                                                        $img_xpos = $img_pos[0];
                                                        $img_ypos = $img_pos[1] ? $img_pos[1] : $img_ypos;
                                                    }
                                                
                                                    /* If there is an uploaded image*/
                                                    if ( get_option( 'image_slider_url'.$i ) ) :
                                            ?>
                                                        <!-- Set the image to transition -->
                                                        <div class="caption <?php echo get_option( 'image_slider_transition'.$i ) ? get_option( 'image_slider_transition'.$i ) : 'lft'; ?>"
                                                          data-x="<?php echo $img_xpos; ?>" 
                                                          data-y="<?php echo $img_ypos; ?>"
                                                          data-speed="700" 
                                                          data-start="1000" 
                                                          data-easing="easeOutExpo">
                                                            <img src="<?php echo get_option( 'image_slider_url'.$i ); ?>" alt="Image <?php echo $i; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>">
                                                        </div>
                                                <?php 
                                                    endif; /* End if ( get_option( 'image_slider_url'.$i ) ) ==> if there is an image */ 
                                                ?>
                                        
                                            <?php 
                                                endif; /* End if ( 2 == get_option( 'image_slider_settings' ) ) ==> if the selected option is 'Image with caption' */
                                            ?>
                                        
                                            <?php 
                                                /* If there is a caption title */
                                                if ( get_option( 'image_slider_title'.$i ) ) :
                                                    
                                                    /* Set default values of variables */
                                                    $title_xpos = 30;
                                                    $title_ypos = 90;
                                                    
                                                    /* If the caption title position is set, change the value of variables */
                                                    if ( get_option( 'image_slider_title_pos'.$i ) ) {
                                                        $title_pos = explode( ',', get_option( 'image_slider_title_pos'.$i ) );
                                                        $title_xpos = $title_pos[0];
                                                        $title_xpos = $title_pos[1] ? $title_pos[1] : $title_ypos;
                                                    }
                                            ?>
                                                    <div class="caption <?php echo get_option( 'image_slider_transition'.$i ) ? get_option( 'image_slider_transition'.$i ) : 'lft'; ?> slide_title_white slide_item_left"
                                                        data-x="<?php echo $title_xpos; ?>"
                                                        data-y="<?php echo $title_ypos; ?>"
                                                        data-speed="400"
                                                        data-start="1500"
                                                        data-easing="easeOutExpo">
                                                        <?php echo get_option( 'image_slider_title'.$i ); ?>
                                                    </div>
                                            <?php 
                                                endif; /* End if ( get_option( 'image_slider_title'.$i ) ) ==> if there is a caption title */
                                            ?>
                                        
                                            <?php 
                                                /* If there is a caption subtitle */
                                                if ( get_option( 'image_slider_subtitle'.$i ) ) :
                                                
                                                    /* Set the default value of variables */
                                                    $subtitle_xpos = 87;
                                                    $subtitle_ypos = 245;
                                                    
                                                    /* If the caption subtitle position is set, change the value of variables */ 
                                                    if ( get_option( 'image_slider_subtitle_pos'.$i ) ) {
                                                        $subtitle_pos = explode( ',', get_option( 'image_slider_subtitle_pos'.$i ) );
                                                        $subtitle_xpos = $subtitle_pos[0];
                                                        $subtitle_ypos = $subtitle_pos[1] ? $subtitle_pos[1] : $subtitle_ypos;
                                                    }
                                            ?>
                                                    <div class="caption <?php echo get_option( 'image_slider_transition'.$i ) ? get_option( 'image_slider_transition'.$i ) : 'lft'; ?> slide_subtitle_white slide_item_left"
                                                      data-x="<?php echo $subtitle_xpos; ?>"
                                                      data-y="<?php echo $subtitle_ypos; ?>"
                                                      data-speed="400"
                                                      data-start="2000"
                                                      data-easing="easeOutExpo">
                                                      <?php echo get_option( 'image_slider_subtitle'.$i ); ?>
                                                    </div>
                                            <?php 
                                                endif; /* End if ( get_option( 'image_slider_subtitle'.$i ) ) ==> if there's a caption subtitle */ 
                                            ?>
                                        
                                            <?php 
                                                /* If there's a caption button */
                                                if ( get_option( 'image_slider_button'.$i ) ) :
                                                    
                                                    /* Set the default values of variables */
                                                    $button_xpos = 87;
                                                    $button_ypos = 345;
                                                    
                                                    /* If the position of caption button is set, change the values of variables */
                                                    if( get_option( 'image_slider_button_pos'.$i ) ) {
                                                        $button_pos = explode(',', get_option('image_slider_button_pos'.$i));
                                                        $button_xpos = $button_pos[0];
                                                        $button_ypos = $button_pos[1] ? $button_pos[1] : $button_ypos;
                                                    }
                                            ?>
                                                    <a class="caption <?php echo get_option( 'image_slider_transition'.$i ) ? get_option( 'image_slider_transition'.$i ) : 'lft'; ?> btn w3-theme-l1 slide_btn slide_item_left" href="#"
                                                      data-x="<?php echo $button_xpos; ?>"
                                                      data-y="<?php echo $button_ypos; ?>"
                                                      data-speed="400"
                                                      data-start="3000"
                                                      data-easing="easeOutExpo">
                                                      <?php echo get_option( 'image_slider_button'.$i ); ?>
                                                    </a>
                                            <?php 
                                                endif; /* End if ( get_option( 'image_slider_button'.$i ) ) ==> if there's a caption button */
                                            ?>
                                        <?php 
                                            endif; /* End if ( 2 == get_option( 'image_slider_settings' ) OR 3 == get_option( 'image_slider_settings' ) ) */
                                        ?>
                                    </li>
                                <?php 
                                    endif; /* if ( get_option( 'image_slider_url'.$i ) ) ==> if there's an image */
                                ?>
                            <?php endfor; ?>
                        </ul>
                        <div class="tp-bannertimer tp-bottom"></div>
                    </div>
                </div>
            </div>
            <!-- END SLIDER -->
    <?php endif; /* End if(get_option('image_slider_on')) */
    
    } /* End of function init() */
    
} /* End of class Image_Slider */

/* End of file */
