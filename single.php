<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Makina
 */

get_header(); ?>

	<main class="main w3-white">
            <div class="container">
                <div class="w3-row-padding  margin-top-15 margin-bottom-15">
                    <div class="w3-col <?php echo ( ! is_active_sidebar( 'sidebar-1' ) && ! is_active_sidebar( 'sidebar-2' ) ) ? 'm12 s12' : 'm9 s12' ?>">
                        <div class="w3-card-4 w3-container padding-top-20">
                        <?php
                        while ( have_posts() ) : the_post();

                            get_template_part( 'template-parts/content', get_post_format() );

                            the_post_navigation();

                            // If comments are open or we have at least one comment, load up the comment template.
                            if ( comments_open() || get_comments_number() ) :
                                    comments_template();
                            endif;

                        endwhile; // End of the loop.
                        ?>
                        </div>
                    </div>
                    <div class="w3-col m3 s12">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
	</main>

<?php
get_footer();
