<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Makina
 */

get_header(); ?>

    <main class="main w3-white">
        <div class="container">
            <div class="w3-row-padding  margin-top-15 margin-bottom-15">
                <div class="w3-col <?php echo ( ! is_active_sidebar( 'sidebar-1' ) && ! is_active_sidebar( 'sidebar-2' ) ) ? 'm12 s12' : 'm9 s12' ?>">
                    <div class="w3-card-4 w3-container padding-top-20">
                        <?php
                        if ( have_posts() ) : ?>

                                <header class="page-header">
                                        <?php
                                                the_archive_title( '<h1 class="page-title">', '</h1>' );
                                                the_archive_description( '<div class="archive-description">', '</div>' );
                                        ?>
                                </header><!-- .page-header -->

                                <?php
                                /* Start the Loop */
                                while ( have_posts() ) : the_post();

                                        /*
                                         * Include the Post-Format-specific template for the content.
                                         * If you want to override this in a child theme, then include a file
                                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                         */
                                        get_template_part( 'template-parts/content', get_post_format() );

                                endwhile;

                                the_posts_navigation();

                        else :

                                get_template_part( 'template-parts/content', 'none' );

                        endif; ?>
                    </div>
                </div>
                <div class="w3-col m3 s12">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </main>

<?php
get_sidebar();
get_footer();
